/*
 * <MNEMONIC - Herigone's System Mnemonics Generator>
 * Copyright (C) <2020>  <Guillermo Maggio>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ar.com.mnemonic.mnemonic;

import android.content.Context;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Mnemonic {

    public static void sherlock(ArrayList<Object> filteredDict, JSONObject fullDict, String inputNumber) throws JSONException {
//        This function takes a number in string format
//        breaks that number in smaller pieces of 3 digits
//        searches for that pieces in fullDict and returns
//        the words that corresponds to the phonetics translation
//        if there is no word for those 3 digits then searches words of 2 digits
//        that is asserted there will be
        if (inputNumber.length() < 3) {
            filteredDict.add(fullDict.get(inputNumber));
        } else {
            String shortenedNumber = inputNumber.substring(0, 3);
            JSONArray wordList = (JSONArray) fullDict.get(shortenedNumber);
            if (wordList.length() == 0) {
                filteredDict.add(fullDict.get(inputNumber.substring(0, 2)));
                sherlock(filteredDict, fullDict, inputNumber.substring(2));
            } else if (inputNumber.length() == 3) {
                filteredDict.add(fullDict.get(inputNumber));
            } else {
                filteredDict.add(fullDict.get(inputNumber.substring(0, 3)));
                sherlock(filteredDict, fullDict, inputNumber.substring(3));
            }
        }
    }

    public static void sherlockFixed(ArrayList<Object> filteredDict, JSONObject fullDict, String inputNumber) throws JSONException {
//        This function takes a number in string format
//        breaks that number in smaller pieces of 2 digits fixed length
//        searches for that pieces in fullDict and returns
//        the words that corresponds to the phonetics translation

        if (inputNumber.length() <= 2) {
            filteredDict.add(fullDict.get(inputNumber));
        } else {
            String shortenedNumber = inputNumber.substring(0, 2);
            filteredDict.add(fullDict.get(shortenedNumber));
            sherlockFixed(filteredDict, fullDict, inputNumber.substring(2));
        }
    }

    public static String inverter(String words, JSONObject graphemes) throws JSONException {
//      This function takes a string of all the words that you want to anti-transform
//      It will search for each pattern in graphemes and it will replace for its numerical value
//      then it will delete all vocals and return the numbers that correspond to that word

        Iterator<String> keys = graphemes.keys();
        while (keys.hasNext()) {
            String graph = keys.next();
            String digit = (String) graphemes.get(graph);

            Pattern pattern = Pattern.compile(graph);
            Matcher matcher = pattern.matcher(words);
            words = matcher.replaceAll(digit);
        }

        Pattern pattern = Pattern.compile("[a-zA-Zàèìòùáéíóúâêîôû-]*");
        Matcher matcher = pattern.matcher(words);
        words = matcher.replaceAll("");
        return words;

    }

    public static ArrayList<String> jsonArray2StringArraylist(Object jObject) throws JSONException {
//      This function will take a json array that contains a list of strings
//      and will return an array list of those strings

        ArrayList<String> outpuList = new ArrayList<String>();
        JSONArray jsonArrayInput = (JSONArray) jObject;
        for (int i = 0; i < jsonArrayInput.length(); i++) {
            outpuList.add(jsonArrayInput.getString(i));
        }
        return outpuList;
    }


    public static JSONObject getJsonFromAssets(Context context, String fileName) throws JSONException {
//      This function will take a file from the assets and will import it over a JSON format
        String jsonString;
        try {
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return new JSONObject(jsonString);
    }
}
