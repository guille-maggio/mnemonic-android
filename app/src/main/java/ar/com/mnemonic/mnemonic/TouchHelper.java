/*
 * <MNEMONIC - Herigone's System Mnemonics Generator>
 * Copyright (C) <2020>  <Guillermo Maggio>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ar.com.mnemonic.mnemonic;

import android.view.MotionEvent;

public class TouchHelper {
    public static final int MIN_DISTANCE = 80;
    public static final int LEFT_TO_RIGHT = 160;
    public static final int RIGHT_TO_LEFT = 161;
    public static final int UP_TO_DOWN = 162;
    public static final int DOWN_TO_UP = 163;
    private static float x1, x2, y1, y2;
    private static int movement;


    public static int checkMovement(MotionEvent event) {
        int action = event.getActionMasked();
        movement = 0;
        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                x1 = event.getX();
                y1 = event.getY();
                break;
            case (MotionEvent.ACTION_MOVE):
                break;
            case (MotionEvent.ACTION_UP):
                x2 = event.getX();
                y2 = event.getY();
                float deltaX = x2 - x1;
                float deltaY = y2 - y1;
                if (Math.abs(deltaX) > Math.abs(deltaY)) {
                    if (deltaX > MIN_DISTANCE) {
                        movement = LEFT_TO_RIGHT;
                    } else if (deltaX < -1 * MIN_DISTANCE) {
                        movement = RIGHT_TO_LEFT;
                    }
                } else {
                    if (deltaY > MIN_DISTANCE) {
                        movement = UP_TO_DOWN;
                    } else if (deltaY < -1 * MIN_DISTANCE) {
                        movement = DOWN_TO_UP;
                    }
                }
                break;
            case (MotionEvent.ACTION_CANCEL):
                break;
            case (MotionEvent.ACTION_OUTSIDE):
                break;
            default:
                break;
        }
        return movement;
    }
}
