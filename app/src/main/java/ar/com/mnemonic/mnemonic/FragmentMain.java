/*
 * <MNEMONIC - Herigone's System Mnemonics Generator>
 * Copyright (C) <2020>  <Guillermo Maggio>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ar.com.mnemonic.mnemonic;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import org.json.JSONException;

import java.util.ArrayList;

import static ar.com.mnemonic.mnemonic.ActivityMain.sherlockFixedLength;
import static ar.com.mnemonic.mnemonic.Mnemonic.*;


public class FragmentMain extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.setBackgroundResource(R.drawable.background_main);

        TextView t = getView().findViewById(R.id.language_view);
        t.setText(LangHelper.getLanguageTitle().toUpperCase());

        view.findViewById(R.id.to_inverter_fragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FragmentMain.this)
                        .navigate(R.id.action_MainFragment_to_InverterFragment);
            }
        });

        view.findViewById(R.id.get_mnemonic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText editText = getActivity().findViewById(R.id.number_to_transform);
                String value = editText.getText().toString();

                ArrayList<Object> outputList = new ArrayList<>();

                if (sherlockFixedLength == false) {
                    try {
                        sherlock(outputList, LangHelper.getFullDict(), value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        sherlockFixed(outputList, LangHelper.getFullDict(), value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Bundle bundle = new Bundle();
                bundle.putInt("size", outputList.size());

                for (int i = 0; i < outputList.size(); i++) {
                    try {
                        bundle.putStringArrayList("item" + i, jsonArray2StringArraylist(outputList.get(i)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                NavHostFragment.findNavController(FragmentMain.this)
                        .navigate(R.id.action_MainFragment_to_MnemonicFragment, bundle);
            }
        });


        view.findViewById(R.id.help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FragmentMain.this)
                        .navigate(R.id.action_MainFragment_to_PhoneticsRulesFragment);
            }
        });


        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int movement = TouchHelper.checkMovement(event);
                switch (movement) {
                    case (TouchHelper.LEFT_TO_RIGHT):
                        view.findViewById(R.id.to_inverter_fragment).performClick();
                        return true;
                    case (TouchHelper.UP_TO_DOWN):
                        view.findViewById(R.id.get_mnemonic).performClick();
                        return true;
                    case (TouchHelper.RIGHT_TO_LEFT):
                        view.findViewById(R.id.help).performClick();
                        return true;
                    default:
                        return true;
                }
            }
        });

    }

}
