/*
 * <MNEMONIC - Herigone's System Mnemonics Generator>
 * Copyright (C) <2020>  <Guillermo Maggio>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ar.com.mnemonic.mnemonic;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Iterator;


public class FragmentPhoneticsRules extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_phonetics_rules, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundResource(R.drawable.background_right);

        view.findViewById(R.id.back_to_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


        LinearLayout rulesLayout = getActivity().findViewById(R.id.rules_layout);
        JSONObject languageRules = LangHelper.getLanguageRules();

        Iterator<String> keys = languageRules.keys();

        while (keys.hasNext()) {
            String digit = keys.next();
            String graphemesForDigit = null;
            try {
                graphemesForDigit = (String) languageRules.get(digit);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            LinearLayout container = (LinearLayout) View.inflate(getActivity(), R.layout.custom_phoneme_element, null);
            final TextView digitView =  container.findViewById(R.id.digit);
            final TextView graphemesView =  container.findViewById(R.id.graphemes);

            digitView.setText(digit);
            graphemesView.setText(graphemesForDigit);
            rulesLayout.addView(container);
        }

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int movement = TouchHelper.checkMovement(event);
                switch (movement) {
                    case (TouchHelper.LEFT_TO_RIGHT):
                        view.findViewById(R.id.back_to_main).performClick();
                        return true;
                    default:
                        return true;
                }
            }
        });
    }
}
